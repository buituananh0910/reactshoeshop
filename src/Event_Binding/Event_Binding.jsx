import React, { Component } from 'react'

export default class Event_Binding extends Component {
    handleClickMe = () => {
        console.log("yes");
    }
    handleSayHello = (name) => {
        console.log(name);
    }
  render() {
    return (
      <div>
        <p>Event_Binding</p>
        <button onClick={this.handleClickMe} className="btn btn-warning" >click me</button>
        <button onClick={() => {this.handleSayHello("BTA")}} className="btn btn-secondary">click to say hello</button>
        </div>
    )
  }
}
