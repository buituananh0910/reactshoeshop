import React, { Component } from "react";
import { connect } from "react-redux";
class DemoReduxMini extends Component {
  render() {
    return (
      <div className="text-center py-5">
        <span className="display-4 text-secondary">{this.props.number}</span>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.number.soLuong,
    isLogin: state.number.isLogin,
  };
};

export default connect(mapStateToProps)(DemoReduxMini);
