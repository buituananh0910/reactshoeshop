import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import Data_Binding from "./Data_Binding/Data_Binding";
import Event_Binding from "./Event_Binding/Event_Binding";
import Conditional_Rendering from "./Conditional_Rendering/Conditional_Rendering";
import Demo_State from "./Demo_State/Demo_State";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import Demo_Prop from "./Demo_Prop/Demo_Prop";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import BaiTapLayoutComponent from "./BaiTapLayoutComponent/BaiTapLayoutComponent";
import DemoReduxMini from "./DemoReduxMini/DemoReduxMini";

function App() {
  return (
    <div className="App">
      {/* <DemoClass></DemoClass> */}

      {/* <DemoFunction /> */}
      {/* <Ex_Layout /> */}

      {/* <Data_Binding /> */}
      {/* <Event_Binding /> */}
      {/* <Conditional_Rendering /> */}
      {/* <Demo_State /> */}
      {/* <RenderWithMap /> */}
      {/* <Demo_Prop /> */}
      <Ex_ShoeShop />
      {/* <BaiTapLayoutComponent /> */}
      {/* <DemoReduxMini /> */}
    </div>
  );
}

export default App;
